<?php
/**
 * @file
 * Viewmode columns style plugin for the Views module.
 */

/**
  * Implements a style type plugin for the Views module.
  */
class viewmode_columns_plugin_style_viewmode_columns extends views_plugin_style {

  /**
   * Set default options
   */
  function option_definition() {
    $options = parent::option_definition();

    $options['breakpoints'] = array('default' => '');

    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    $form['breakpoints'] = array(
      '#type' => 'textfield',
      '#title' => t('Breakpoints'),
      '#description' => t('Comma separated breakpoints, e.g. \'1,2,3\'.'),
      '#default_value' => $this->options['breakpoints'],
    );
  }

  /**
   * Validate the options form.
   */
  function options_validate(&$form, &$form_state) {
    $breakpoints = $form_state['values']['style_options']['breakpoints'];

    if ( empty($breakpoints) ) {
      form_error($form['breakpoints'], t('Breakpoints may not be empty.'));
    }
    else {
      $values = explode(',', $breakpoints);
      foreach ( $values as $value ) {
        if ( !is_numeric($value) ) {
          form_error($form['breakpoints'], t('Only (comma separated) integers are allowed, e.g. \'1,2,3\'.'));
        }
      }
    }
  }
}
