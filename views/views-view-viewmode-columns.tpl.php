<?php
/**
 * @file views-view-viewmode-columns.tpl.php
 *
 * @ingroup views_templates
 */
?>
<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div class="<?php print $classes_array[$id]; ?>">
    <div class="vc-inner">
      <?php print $row; ?>
    </div>
  </div>
<?php endforeach; ?>
