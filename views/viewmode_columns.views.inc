<?php
/**
 * @file
 * Contains core functions for the Views module support.
 */

/*
 * Implements hook_views_plugins().
 *
 * This function annnounces the style plugin for viewmode columns.
 */
function viewmode_columns_views_plugins() {
  return array(
    'style' => array(
      // Style plugin for the navigation.
      'viewmode_columns' => array(
        'title' => t('Viewmode columns'),
        'help' => t('Display the results in a viewmode column grid.'),
        'handler' => 'viewmode_columns_plugin_style_viewmode_columns',
        'theme' => 'views_view_viewmode_columns',
        'theme path' => drupal_get_path('module', 'viewmode_columns') . '/views',
        'uses row plugin' => TRUE,
        'uses row class' => FALSE,
        'uses options' => TRUE,
        'uses grouping' => FALSE,
        'uses fields' => FALSE,
        'type' => 'normal',
        'even empty' => FALSE,
      ),
    ),
  );
}
