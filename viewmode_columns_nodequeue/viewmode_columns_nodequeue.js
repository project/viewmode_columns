(function ($) {

  Drupal.behaviors.viewmodeColumnsNodequeueDrag = {
    attach: function(context) {
      $('.nodequeue-dragdrop').each(function() {
        var table_id = $(this).attr('id');
        viewmodeColumnsDrawPreview(table_id);
      });
    }
  };

  Drupal.behaviors.viewmodeColumnsNodequeueViewmodeChange = {
    attach: function(context) {
      $('.nodequeue-dragdrop select').change(function() {
        var $table = $(this).parent().parent().parent().parent().parent().parent().find('.nodequeue-dragdrop:first');
        var table_id = $table.attr('id');
        viewmodeColumnsDrawPreview(table_id);
      });
    }
  };

  Drupal.behaviors.viewmodeColumnsNodequeueReverse = {
    attach: function(context) {
      $('#edit-actions-reverse').click(function() {
        var $table = $(this).parent().parent().find('.nodequeue-dragdrop:first');
        var table_id = $table.attr('id');

        viewmodeColumnsDrawPreview(table_id);

        return false;
      });
    }
  };

  Drupal.behaviors.viewmodeColumnsNodequeueShuffle = {
    attach: function(context) {
      $('#edit-actions-shuffle').click(function() {
        var $table = $(this).parent().parent().find('.nodequeue-dragdrop:first');
        var table_id = $table.attr('id');

        viewmodeColumnsDrawPreview(table_id);

        return false;
      });
    }
  };

  Drupal.behaviors.viewmodeColumnsNodequeueClear = {
    attach: function(context) {
      $('#edit-actions-clear').click(function() {
        var $table = $(this).parent().parent().find('.nodequeue-dragdrop:first');
        var table_id = $table.attr('id');

        viewmodeColumnsDrawPreview(table_id);

        return false;
      });
    }
  };

  Drupal.behaviors.viewmodeColumnsNodequeueRemoveNode = {
    attach: function(context) {
      $('a.nodequeue-remove').click(function() {
        $(this).parent().parent().fadeOut('fast', function() {
          var $table = $(this).parent().parent();
          var table_id = $table.attr('id');

          viewmodeColumnsDrawPreview(table_id);
        });

        return false;
      });
    }
  };

  Drupal.behaviors.viewmodeColumnsNodequeueHover = {
    attach: function(context) {
      $('.nodequeue-dragdrop tbody tr:not(:hidden)').hover(function() {
        $(this).addClass('viewmode-columns-active');
        var row = $(this).parent().children().index($(this));
        $('.viewmode_columns_nodequeue_preview_container div:nth-child(' + (row + 1) + ')').addClass('viewmode-columns-active');
      }, function() {
        $(this).removeClass('viewmode-columns-active');
        var row = $(this).parent().children().index($(this));
        $('.viewmode_columns_nodequeue_preview_container div:nth-child(' + (row + 1) + ')').removeClass('viewmode-columns-active');
      });
    }
  };

  Drupal.behaviors.viewmodeColumnsPreviewHover = {
    attach: function(context) {
      $('.viewmode_columns_nodequeue_preview_container').children('div').hover(function() {
        $(this).addClass('viewmode-columns-active');
        var row = $(this).parent().children().index($(this));
        $('.nodequeue-dragdrop tbody tr:not(:hidden):nth-child(' + (row + 1) + ')').addClass('viewmode-columns-active');
      }, function() {
        $(this).removeClass('viewmode-columns-active');
        var row = $(this).parent().children().index($(this));
        $('.nodequeue-dragdrop tbody tr:not(:hidden):nth-child(' + (row + 1) + ')').removeClass('viewmode-columns-active');
      });
    }
  };

  /**
   * Draws preview.
   */
  function viewmodeColumnsDrawPreview(table_id) {
    var rows = $('#' + table_id + ' tbody tr:not(:hidden)').get();
    var output = '';

    $.each(rows, function(i, row) {
      var nodetype = $(this).find('select').parent().parent().attr('class').replace('nodequeue-viewmode-', '');
      var viewmode = $(this).find('select').val();
      var cols = Drupal.settings.viewmode_columns_nodequeue.columns[nodetype + '|' + viewmode];
      if ( cols === undefined ) {
        cols = 3;
      }

      var unit = 180 / Drupal.settings.viewmode_columns_nodequeue.row_columns;

      output = output + '<div class="viewmode_columns_nodequeue_preview_block" style="width: ' + (cols * unit) + 'px;"><div class="inner">' + (i + 1) + '</div></div>';
    });

    var container = $('.viewmode_columns_nodequeue_preview_container').size();
    if (  container === 0 ) {
      output = '<div class="clearfix viewmode_columns_nodequeue_preview_container">' + output + '</div>';

      var width = $('#' + table_id).width() - 220;
      $('#' + table_id).width(width);
      $('#' + table_id).parent().find('.tabledrag-toggle-weight-wrapper').width(width);
      $('#' + table_id).before(output);
    }
    else {
      $('.viewmode_columns_nodequeue_preview_container').html(output);
    }
  }

})(jQuery);
