<?php
/**
 * @file
 * Admin settings for the viewmode columns module.
 */

/**
 * Admin settings form
 */
function viewmode_columns_admin_settings($form, &$form_state) {
  $form = array();

  $bundles = viewmode_columns_bundle_list();

  $form['counter'] = array('#tree' => TRUE);

  $defaults = variable_get('viewmode_columns_settings', array());

  if ( $bundles ) {
    foreach ($bundles as $bundle => $poperties) {
      $label = $poperties['label'];
      foreach ( $poperties['viewmodes'] as $viewmode => $name ) {
        $viewmode_id = $bundle . '|' . ($viewmode == '' ? 'default' : $viewmode);

        $form['columns'][$viewmode_id]['#viewmode_id'] = $viewmode_id;
        $form['columns'][$viewmode_id]['bundle']['#markup'] = $label;
        $form['columns'][$viewmode_id]['viewmode']['#markup'] = $name;

        $form['counter'][$viewmode_id] = array(
          '#type' => 'select',
          '#title' => t('Columns'),
          '#title_display' => 'invisible',
          '#default_value' => isset($defaults[$viewmode_id]) ? $defaults[$viewmode_id] : 0 ,
          '#options' => range(0, 15),
        );
      }
    }
  }

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

/**
 * Save settings of admin form
 */
function viewmode_columns_admin_settings_submit($form, &$form_state) {
  $columns = $form_state['values']['counter'];
  variable_set('viewmode_columns_settings', $columns);

  drupal_set_message(t('The configuration options have been saved.'));
}

/**
 * Theme admin settings form (table)
 */
function theme_viewmode_columns_admin_settings($variables) {
  $form = $variables['form'];

  // Table headers.
  $header = array(
    t('Bundle'),
    t('Viewmode'),
    t('Columns'),
  );

  // Display table.
  $rows = array();
  if ( isset($form['columns']) ) {
    foreach (element_children($form['columns']) as $viewmode_id) {
      $rows[] = array(
        array('data' => drupal_render($form['columns'][$viewmode_id]['bundle']), 'align' => 'left'),
        array('data' => drupal_render($form['columns'][$viewmode_id]['viewmode']), 'align' => 'left'),
        array('data' => drupal_render($form['counter'][$viewmode_id]), 'align' => 'center'),
      );
    }
  }

  $output  = theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('No bundles and viewmodes.')));
  $output .= drupal_render_children($form);

  return $output;
}
